-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2016 at 03:56 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library_records`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_records`
--

CREATE TABLE `book_records` (
  `ISBN` varchar(14) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Author` varchar(40) NOT NULL,
  `Status` varchar(40) NOT NULL,
  `State` varchar(40) NOT NULL,
  `Number` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_records`
--

INSERT INTO `book_records` (`ISBN`, `Title`, `Author`, `Status`, `State`, `Number`) VALUES
('1004', 'llklk', 'klklk', 'Borrowed', 'Old', 12),
('212121', 'hjhj', 'jhhjh', 'jjhj', 'hjjh', 33),
('23232332', 'The Archers', 'Trian Tinoli', 'Returned', 'New', 156),
('232455', 'Chawey', 'Hassan Nigrosani', 'Borrowed', 'Old', 22),
('34434', 'Weed', 'Daris Nan', 'Returned', 'Old', 23),
('4545', 'Gunner', 'George Rang', 'Borrowed', 'New', 23),
('454545', 'Fools Day', 'Daniel Copper', 'Borrowed', 'New', 29),
('89898', 'Hangover', 'Hanna Vienda', 'Borrowed', 'New', 23);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_records`
--
ALTER TABLE `book_records`
  ADD PRIMARY KEY (`ISBN`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
