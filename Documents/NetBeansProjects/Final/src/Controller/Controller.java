/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Adb;
import View.Add;
import View.Records;
import View.Update;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author owner
 */
public class Controller implements ActionListener {

    Adb db;
    Records gui;
    Add add;
    Update update;
    public static final String SPECIAL_CHARS = "/*!@#$%^&*()\"{}_[]|\\?/<>,.-";
    public static final int ID_LENGTH = 8;

    public Controller(Adb db, Records gui, Add add, Update update) {
        this.db = db;
        this.gui = gui;
        this.add = add;
        this.update = update;
    }

    /**
     * control function that accesses all used view objects
     */
    public void control() {
        //creating listeners for all used buttons
        gui.getAdd().addActionListener(this);
        gui.getPopulate().addActionListener(this);
        gui.getUpdate().addActionListener(this);
        gui.getDelete().addActionListener(this);
        gui.getSearch().addActionListener(this);
        gui.getReport().addActionListener(this);
        gui.getSearchButton().addActionListener(this);
        add.getAdder().addActionListener(this);
        update.getUpdater().addActionListener(this);
        add.getExit().addActionListener(this);
        update.getUpexit().addActionListener(this);
        gui.getAddmenu().addActionListener(this);
        gui.getUpdatemenu().addActionListener(this);
        //setting gui to visible
        gui.setVisible(true);
    }
    /**
     * Invokes appropriate button and action for expected events
     * @param event event that was heard 
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        //branches that allow any of the listening buttons to perform a tasked if they are the source of an event
        if (event.getSource() == gui.getDelete()) {
            int affirm = -1;
            DefaultTableModel model = (DefaultTableModel) gui.getTable().getModel();

            if (gui.getTable().getSelectedRow() == -1) {
                if (gui.getTable().getRowCount() == 0) {
                    gui.getError().setText("Please populate the table first , then select a record");
                } else {
                    gui.getError().setText("Please select a record to delete");
                }
            } else {
                String id = (String) model.getValueAt(gui.getTable().getSelectedRow(), 0);
                try {
                    affirm = db.recordExists(id);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (affirm == 0) {
                    String state = "ISBN Does Not Exist In Database";
                    JOptionPane.showMessageDialog(null, state);
                } else if (affirm == 1) {
                    try {
                        try {
                            if (db.delete(id) != -1) {
                                String state = "Book Record Deleted";
                                JOptionPane.showMessageDialog(null, state);
                            } else {
                                String state = "Book Record Could not be Deleted";
                                JOptionPane.showMessageDialog(null, state);
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InstantiationException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }

        } else if (event.getSource() == gui.getPopulate()) {
            ResultSet rs = null;
            try {
                rs = db.getAllRecords();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            while (gui.getTable().getRowCount() > 0) {
                ((DefaultTableModel) gui.getTable().getModel()).removeRow(0);
            }
            int columns = 0;
            try {
                columns = rs.getMetaData().getColumnCount();
            } catch (SQLException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                while (rs.next()) {
                    Object[] row = new Object[columns];
                    for (int i = 1; i <= columns; i++) {
                        row[i - 1] = rs.getObject(i);
                    }
                    ((DefaultTableModel) gui.getTable().getModel()).insertRow(rs.getRow() - 1, row);
                }
            } catch (SQLException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (event.getSource() == gui.getSearchButton()) {
            String isbn = gui.getSearch().getText();

            if (isbn == null) {
                gui.getError().setText("Please input an ISBN");
            } else if (!(Integer.valueOf(isbn) instanceof Integer)) {
                gui.getError().setText("Please input a valid ISBN");
            } else {
                try {
                    ResultSet rs = db.getRecord(isbn);
                    while (gui.getTable().getRowCount() > 0) {
                        ((DefaultTableModel) gui.getTable().getModel()).removeRow(0);
                    }
                    int columns = 0;
                    try {
                        columns = rs.getMetaData().getColumnCount();
                    } catch (SQLException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        while (rs.next()) {
                            Object[] row = new Object[columns];
                            for (int i = 1; i <= columns; i++) {
                                row[i - 1] = rs.getObject(i);
                            }
                            ((DefaultTableModel) gui.getTable().getModel()).insertRow(rs.getRow() - 1, row);
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    try {
                        rs.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (event.getSource() == gui.getReport()) {
            ResultSet rs = null;
            List data = new ArrayList();
            try {
                rs = db.getAllRecords();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            String path = "C:\\Users\\owner\\Documents\\NetBeansProjects\\Final\\src\\Controller";
            String cols = "%20s %20s %20s %20s %20s %20s\r\n";
            FileWriter f;
            try {
                try {
                    f = new FileWriter(new File(path, "BookRecords.txt"));
                    f.write("                                                               Library Book Records\n");
                    System.out.println("                                                          Library Book Records\n");
                    f.write(String.format(cols, "ISBN", "Title", "Author", "Status", "State", "Number"));
                    System.out.println(String.format(cols, "ISBN", "Title", "Author", "Status", "State", "Number"));
                } catch (IOException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
                while (rs.next()) {
                    String isbn = rs.getString("ISBN");
                    String title = rs.getString("Title");
                    String author = rs.getString("Author");
                    String status = rs.getString("Status");
                    String state = rs.getString("State");
                    String number = rs.getString("Number");
                    FileWriter fw = null;
                    try {
                        File file = new File(path, "BookRecords.txt");
                        fw = new FileWriter(file, true);
                        fw.write(String.format(cols, isbn, title, author, status, state, number));
                        System.out.println(String.format(cols, isbn, title, author, status, state, number));
                        fw.flush();
                        fw.close();
                    } catch (IOException e) {
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception e) {
                System.out.println(e);
            }
        } else if (event.getSource() == gui.getAdd()||event.getSource() == gui.getAddmenu()) {
            add.setVisible(true);
        } else if (event.getSource() == gui.getUpdate()||event.getSource() == gui.getUpdatemenu()) {
            update.setVisible(true);
        } 
        else if (event.getSource() == add.getAdder()) {
            try {
                db.addRecord(add.getTx1().getText(), add.getTx2().getText(),
                        add.getTx3().getText(), add.getTx4().getText(), add.getTx5().getText(),
                        Integer.parseInt(add.getTx6().getText()));
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (event.getSource() == update.getUpdater()) {
            try {
                db.updateRecord(update.getUptx1().getText(), update.getUptx2().getText(),
                        update.getUptx3().getText(), update.getUptx4().getText(), update.getUptx5().getText(),
                        Integer.parseInt(update.getUptx1().getText()));
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (event.getSource() == add.getExit()) {
            add.dispose();
        } else if (event.getSource() == update.getUpexit()) {
            update.dispose();
        }
    }
}
