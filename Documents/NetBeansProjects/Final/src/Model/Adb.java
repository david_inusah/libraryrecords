/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Haq
 * @version 1
 */
public class Adb {
    
    //static variables of class
    public static final String DATABASE = "library_records";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "";

    public static String dataConnection = "jdbc:mysql://localhost/" + DATABASE;
    
     /**
     * Connects to a database 
     * @return statement
     * @throws ClassNotFoundException catches errors for unfound class
     * @throws InstantiationException catches errors of instantiation
     * @throws IllegalAccessException catches errors of access
     */
    private static Statement connect () throws InstantiationException, ClassNotFoundException, IllegalAccessException{
         Statement query = null;
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(dataConnection,USERNAME,PASSWORD);
            query = conn.createStatement();
    } catch (SQLException ex) {
            Logger.getLogger(Adb.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println(ex);
            System.exit(0);}
        return query;
    }
    
    /**
     * Add a record to a database
     * @param isbn ISBN of book
     * @param title title of book
     * @param author author of book
     * @param status whether book is borrowed or not
     * @param state state of book
     * @param number number of books available
     * @return boolean
     * @throws ClassNotFoundException catches errors for unfound class
     * @throws InstantiationException catches errors of instantiation
     * @throws IllegalAccessException catches errors of access
     */
    public static boolean addRecord(String isbn, String title, String author, String status, String state, int number) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        boolean duplicateKey = false;
            try {
                Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(dataConnection,USERNAME,PASSWORD);
            PreparedStatement q = conn.prepareStatement("Insert into book_records set ISBN=?,Title=?,Author=?,Status=?,State=?,Number=?");
             q.setString(1, isbn);
            q.setString(2, title);
            q.setString(3, author);
            q.setString(4, status);
            q.setString(5, state);
            q.setInt(6, number);
            q.execute();
            } catch (SQLException e) {
                if (e instanceof SQLIntegrityConstraintViolationException) {
                    duplicateKey = true;
                    String state2 = "Invalid Input, Duplicate Key";
                    JOptionPane.showMessageDialog(null, state2);
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid Input");
                }
            }
        if (duplicateKey == false) {
            String state1 = "Your Record has been Entered";
            JOptionPane.showMessageDialog(null, state1);
        }
        return duplicateKey;
    }
    
    /**
     * Retrieves a specified record from the database
     * @param isbn the ISBN of the book to retrieve 
     * @return ResultSet
     * @throws ClassNotFoundException catches errors for unfound class
     * @throws InstantiationException catches errors of instantiation
     * @throws IllegalAccessException catches errors of access
     * @throws SQLException catches sql query errors
     */
     public static ResultSet getRecord(String isbn) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        ResultSet rs = null;
        Statement getQuery = connect();
        rs = getQuery.executeQuery("select * from book_records where ISBN = " + isbn + ";");
         return rs;
    }
     /**
     * Retrieves all records from the database
     * @return ResultSet 
     *@throws ClassNotFoundException catches errors for unfound class
     * @throws InstantiationException catches errors of instantiation
     * @throws IllegalAccessException catches errors of access
     * @throws SQLException catches sql query errors
     */
    public static ResultSet getAllRecords() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        ResultSet rs;
        Statement getQuery = connect();
        rs = getQuery.executeQuery("SELECT * FROM book_records;");
        return rs;
    }
    /**
     * Deletes a specified record from the database
     * @param isbn the ISBN of the book to delete 
     * @return ResultSet
     * @throws ClassNotFoundException catches errors for unfound class
     * @throws InstantiationException catches errors of instantiation
     * @throws IllegalAccessException catches errors of access
     * @throws SQLException catches sql query errors
     */
    public static int delete(String isbn) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        int x = -1;
        Statement addQuery = connect();
            x = addQuery.executeUpdate("delete from book_records where ISBN = " + isbn + ";");
        return x;
    }
    
    /**
     * Updates a specified record in the database
     * @param isbn the ISBN of the book to update 
     * @param title new title of book
     * @param author new author of book
     * @param status new status
     * @param state new state of book
     * @param number new number of books available
     * @throws ClassNotFoundException catches errors for unfound class
     * @throws InstantiationException catches errors of instantiation
     * @throws IllegalAccessException catches errors of access
     * @throws SQLException catches sql query errors
    */
    public static void updateRecord(String isbn, String title, String author, String status, String state, int number) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        try {
                Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(dataConnection,USERNAME,PASSWORD);
            PreparedStatement q = conn.prepareStatement("UPDATE book_records set Title=?,Author=?,Status=?,State=?,Number=? WHERE ISBN=?");
            q.setString(1, title);
            q.setString(2, author);
            q.setString(3, status);
            q.setString(4, state);
            q.setInt(5, number);
             q.setString(6, isbn);
            q.execute();
            JOptionPane.showMessageDialog(null, "Update Successful");
            } catch (SQLException e) {            
                    JOptionPane.showMessageDialog(null, "An error occured. Make sure the ISBN already exists");
                }
    }

    /**
     * Checks whether a specified record exists within the database
     * @param isbn the ISBN of the book to delete 
     * @return int
     * @throws ClassNotFoundException catches errors for unfound class
     * @throws InstantiationException catches errors of instantiation
     * @throws IllegalAccessException catches errors of access
     * @throws SQLException catches sql query errors
     */
    public static int recordExists(String isbn) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        int x = -1;
        Statement addQuery = connect();
            x = addQuery.executeUpdate("UPDATE book_records SET ISBN = " + isbn + " where ISBN = " + isbn + ";");
        return x;
    }
}

