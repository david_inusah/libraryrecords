/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Controller.Controller;
import Model.Adb;
import View.Add;
import View.Records;
import View.Update;
import javax.swing.SwingUtilities;

/**
 *
 * @author owner
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {      
                
                Adb model = new Adb();
                Records view = new Records();
                Add view1 = new Add(); 
                Update view2 = new Update(); 
                Controller controller = new Controller(model,view, view1, view2);
                controller.control();
                
                
            }
        });  
    }
    
}
