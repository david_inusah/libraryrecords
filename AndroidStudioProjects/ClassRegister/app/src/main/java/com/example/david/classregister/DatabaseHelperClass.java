package com.example.david.classregister;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by owner on 11/22/2016.
 */

public class DatabaseHelperClass  extends SQLiteOpenHelper {

    //Declare the variables. Define the name of our database
    public static final String DATABASE_NAME = "student.db";
    public static final String TABLE_NAME = "student_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NAME";
    public static final String COL_3 = "SURNAME";
    public static final String COL_4 = "MARKS";

    //When ever this constructor is called the Database would be created, to make it simple lets only use the Context
    public DatabaseHelperClass (Context context) {
        // In the super method the argument (context, name, factory, version) will be replaced to have the below
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    //When ever you call the oncreate the table would be created
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //This method takes sql querry as its argument
        sqLiteDatabase.execSQL("create table " + TABLE_NAME + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, SURNAME TEXT, MARKS INTEGER) ");
    }

    //In the event of any upgrade
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXIST " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
    public boolean insertData (String name, String surname, String marks){
        //Create an instance of sqllite a lot of insertion into database. Db is not opened until this is called
        SQLiteDatabase db = this.getWritableDatabase();
        //We need the instance of a class called content value, Creates an empty set of values using the default initial size
         ContentValues contentValues = new ContentValues();
        //now we want to put some data into the columns, it takes 2 argument first is the column name and second is the value itself
        contentValues.put(COL_2, name);
        contentValues.put(COL_3, surname);
        contentValues.put(COL_4, marks);
        // Now we can insert our data using our db instance, it takes 3 arguments (Tablename, nullColumnHack, Contentvalues)
        long result = db.insert(TABLE_NAME, null, contentValues);
        //How do we know that the insert was correctly done, 
        if (result == -1)
            return false;
        else
            return true;
    }

    }
